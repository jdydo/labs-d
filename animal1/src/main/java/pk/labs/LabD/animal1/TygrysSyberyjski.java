package pk.labs.LabD.animal1;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import org.osgi.service.component.ComponentContext;
import pk.labs.LabD.contracts.Animal;
import pk.labs.LabD.contracts.Logger;

public class TygrysSyberyjski implements Animal {

    static TygrysSyberyjski instance;

    private final String species;
    private final String name;
    private String status;

    public TygrysSyberyjski() {
        this.species = "Tygrys";
        this.name = "Tygrys syberyjski";
        this.status = "Bezczynny";
    }

    public static TygrysSyberyjski get() {
        return instance;
    }

    @Override
    public String getSpecies() {
        return this.species;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    public void start(ComponentContext context) {
        instance = new TygrysSyberyjski();
        System.out.print("Tygrys - ON");
        logger.log(this, "Tygrys - ON");
    }

    public void stop(ComponentContext context) {
        instance = null;
        System.out.print("Tygrys - OFF");
        logger.log(this, "Tygrys - OFF");
    }

    private Logger logger;

    protected void bindLogger(Logger logger) {
        this.logger = logger;
    }

    protected void unbindLogger(Logger logger) {
        this.logger = null;
    }
}
