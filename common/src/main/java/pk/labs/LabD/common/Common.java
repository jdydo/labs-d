package pk.labs.LabD.common;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import org.osgi.service.component.ComponentContext;
import pk.labs.LabD.contracts.Animal;
import pk.labs.LabD.contracts.Logger;

public class Common implements Animal {

    static Common instance;

    private String species;
    private String name;
    private String status;

    @Override
    public String getSpecies() {
        return this.species;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    protected void activate(ComponentContext context) {
        instance = new Common();
        logger.log(this, "Common - ON");
    }

    protected void deactivate(ComponentContext context) {
        instance = null;
        logger.log(this, "Common - OFF");
    }

    private Logger logger;

    protected void bindLogger(Logger logger) {
        this.logger = logger;
    }

    protected void unbindLogger(Logger logger) {
        this.logger = null;
    }
}
