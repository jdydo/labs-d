package pk.labs.LabD.animal3;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import org.osgi.service.component.ComponentContext;
import pk.labs.LabD.contracts.Animal;
import pk.labs.LabD.contracts.Logger;

public class SlonAfrykanski implements Animal {

    static SlonAfrykanski instance;

    private final String species;
    private final String name;
    private String status;

    public SlonAfrykanski() {
        this.species = "Slon";
        this.name = "Slon Afrykanski";
        this.status = "Bezczynny";
    }

    public static SlonAfrykanski get() {
        return instance;
    }

    @Override
    public String getSpecies() {
        return this.species;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getStatus() {
        return this.status;
    }

    @Override
    public void setStatus(String status) {
        this.status = status;
    }

    private final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }
    
    protected void activate(ComponentContext context) {
        instance = new SlonAfrykanski();
        logger.log(this, "Slon - ON");
    }

    protected void deactivate(ComponentContext context) {
        instance = null;
        logger.log(this, "Slon - OFF");
    }

    private Logger logger;

    protected void bindLogger(Logger logger) {
        this.logger = logger;
    }

    protected void unbindLogger(Logger logger) {
        this.logger = null;
    }
}